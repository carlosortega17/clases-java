# Clases Java

## Introduccion
### Que es Java?

Java es un lenguaje de programacion multiparadigma y multiplataforma.<br>

Es utilizado en<br>
- Aplicaciones moviles
- Aplicaciones de escritorio
- Videojuegos
- Bases de datos
- Servidores web

### Por que usar java?

- Es multiplataforma por lo tanto es compatible con Windows, Linux y MacOS
- Es uno de los lenguajes de programacion mas popular del mundo
- Es orientado a objetos
- Es de codigo abierto
- Es muy parecido a lenguajes como C++ y C#

## Ejemplo practico

Como se menciona en la introduccion Java es un lenguaje de programacion 
multiplataforma por lo tanto se puede instalar de forma sencilla en nuestro 
equipo de computo. Invito al lector a investigar por algun medio externo
como instalar el OpenJDK de Java en su version 11 o 17 y agregarlo al Path de su sistema operativo.<br>

Una vez instalado y configurado en el Path, en su terminal debe escribir lo siguiente:
```bash
java -version
```
y despues:
```bash
javac -version
```
Si ninguno de los comandos nos da error quiere decir que nuestro JDK esta instalado correctamente.<br>

### Visual Studio Code

Al instalar java tambien debemos instalar Visual studio code para poder escribir algo de codigo en este caso una hola mundo basico.

```java
class Main {
  public static void main(String[] args) {
    System.out.println("Hola mundo!");
  }
}
```

Para poder ejecutar este codigo es necesario a travez de la consola escribir